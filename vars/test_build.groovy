def call(body) {
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

node ('master') {
    try {
        stage ('checkout')
        checkout([
        $class: 'GitSCM',
        branches: [[name: "*/${config.gitBranch}"]],
        doGenerateSubmoduleConfigurations: false,
        extensions: [
            [$class: 'CleanBeforeCheckout'],
        ],
        gitTool: 'Default',
        submoduleCfg: [],
        userRemoteConfigs: [[
            url: "${config.gitUrl}"
        ]]
    ])

    stage('create new version'){
      def manifest = readFile("${WORKSPACE}/manifest.json")
      def json = new groovy.json.JsonSlurperClassic().parseText(manifest)
      def builder = new groovy.json.JsonBuilder(json)
      def version = builder.content.app.version
      def version_list = version.toList()
      version_list[-1] = version_list[-1].toInteger() + 1
      updated_version = version_list.join(',')
      updated_version = updated_version.replaceAll(",","")
      builder.content.app.version = updated_version
      new File("${WORKSPACE}/manifest.json").write(builder.toPrettyString())
    }

    stage("BUILD"){
        sh """ set +o xtrace;
            g++ -c src/test_loop.cpp
            mkdir -p bin
            mv test_loop.o bin/test_loop_${updated_version}.o
        """
    }

    stage('publish release branch') {
        sh """ set +o xtrace;
            git config --global user.name "vadymt1"
            git config --global user.email vadym@example.com
            git checkout -b ${updated_version}_release_\$(date +"%m-%d-%y")
            git add .
            git commit -m "[created version ${updated_version}]"
            git push origin ${updated_version}_release_\$(date +"%m-%d-%y")
        """
    }

    if (production_release == 'true') {
        stage('PR to master preparation') {
            sh """ set +o xtrace;
                curl -X POST -H "Content-Type: application/json" -u vadymt1:Aa123456 https://bitbucket.org/api/2.0/repositories/vadymt1/test_build_repo/pullrequests  -d '{ "title": "Merge version branch to master", "description": "production release", "source": { "branch": { "name": "'${updated_version}_release_\$(date +'%m-%d-%y')'" }, "repository": { "full_name": "vadymt1/test_build_repo" } }, "destination": { "branch": { "name": "${config.gitBranch}" } }, "reviewers": [ { "username": "${config.reviewer}" } ], "close_source_branch": false }'
            """
        }
    }
    else {
      println "production release was not selected, pr to master will be skipped"
    }
    } catch (e) {
        currentBuild.result = "FAILURE"
          throw e
      } finally {
          deleteDir()
      }
   }
}
